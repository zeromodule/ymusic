var system = require('system');
var page = require('webpage').create();

page.open(system.args[1], function () {
    var data = page.evaluate(function () {

        var artists = [];

        $("div.b-artist-group a.b-link").each(
            function (i) {
                artists.push($(this).attr('href') + ';' + $(this).text());
            }
        );

        return artists;
    });

    data.forEach(function(el){console.log(el);});

    phantom.exit();

});