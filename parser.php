<?php

ini_set('max_execution_time', 0);

$artists = array();

$mysqli = new mysqli('127.0.0.1', 'root', '0x00h1', 'mymusic');

$mysqli->query("SET NAMES utf8");

// получаем список букв, цифр и символов, на которые начинаются названия исполнителей
//exec('phantomjs ymusic.parser.alphabet.js', $alphabet_links);

$alink = $argv[1];

    $group = 0;
    // внутри каждой буквы есть группы типа F-Fb, Fc-Fi, Fj-Fo, Fp-Fr, Fs-Fz. Если групп нет (т.е. группа одна),
    // то /group/0 всё равно работает, что хорошо
    do
    {

		$links = null; // !!!!!!!!!!!!!!!!!!!!!!!

        exec('phantomjs ymusic.parser.signpages.js ' . $alink . '/group/' . $group, $links);

		if(empty($links)){

			echo "No more groups in this letter\n";

		}else{

			$page = 0;
			// идём по страничкам. Здесь та же ситуация, если страница всего одна, то ?page=0 работает корректно
			do
			{
				$inner_links = null; // !!!!!!!!!!!!!!!!!!!!!1

				$url = $alink . '/group/' . $group . '?page=' . $page;
				echo $url . " ...";

				exec('phantomjs ymusic.parser.signpages.js ' . $url, $inner_links);

				if(!empty($inner_links))
					echo "ok, ".count($inner_links)." artists found\n";
				else
					echo "no data, switching to next group\n";

				$q_base = "INSERT INTO artists (`name`, `yurl`, `pageurl`) VALUES ";

				foreach($inner_links as $ael)
				{
					$aelr = explode(';', $ael);
					list($u, $an) = $aelr;

					$an = $mysqli->escape_string($an);
					$u = $mysqli->escape_string($u);
					$url = $mysqli->escape_string($url);

					$q =  $q_base . " ('{$an}', '{$u}', '{$url}')";
					if(FALSE === $mysqli->query($q)){
						echo "ERROR: Query $q failed\n";
						file_put_contents("./logs/error.log", "ERROR: Query $q failed\n", FILE_APPEND);
					}

				}

				$page++;

				sleep(1);

			}while(!empty($inner_links));

			$group++;
		}

    }while(!empty($links));